# Tatic.io



## Getting started

Tatic.io offers a great suite of managed OpenSearch solutions including both single server high availability and dedicated clusters for larger setups.


Pricing starts at just 39€ and comes with plenty of memory to get your project started whether you are using it for log management or application persistency.

- [ ] [Go to Tatic.io - OpenSearch Managed](https://tatic.io) 
